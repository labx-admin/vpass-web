import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountComponent } from './modules/account/account.component';
import { AdminAccountComponent } from './modules/admin-account/admin-account.component';
import { AdminComponent } from './modules/admin/admin.component';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import { DownloadComponent } from './modules/download/download.component';
import { LandingPageComponent } from './modules/landing-page/landing-page.component';
import { MainComponent } from './modules/main/main.component';
import { NotificationsComponent } from './modules/notifications/notifications.component';
import { PrivacyComponent } from './modules/privacy/privacy.component';
import { ResetpasswordComponent } from './modules/resetpassword/resetpassword.component';
import { ResultsComponent } from './modules/results/results.component';
import { SetpasswordComponent } from './modules/setpassword/setpassword.component';
import { SignupComponent } from './modules/signup/signup.component';
import { TermsComponent } from './modules/terms/terms.component';
import { TestdetailsComponent } from './modules/testdetails/testdetails.component';
import { TestresultsComponent } from './modules/testresults/testresults.component';


const routes: Routes = [
  { path: 'home', component: LandingPageComponent },
  { path: 'signup/:page', component: SignupComponent },
  { path: 'main', component: MainComponent },
  { path: 'notifications', component: NotificationsComponent },
  { path: 'account', component: AccountComponent },
  { path: 'setpassword', component: SetpasswordComponent },
  { path: 'testresults', component: TestresultsComponent },
  { path: 'termsandconditions', component: TermsComponent },
  { path: 'privacypolicy', component: PrivacyComponent },
  { path: 'downloadnow', component: DownloadComponent },
  { path: 'result', component: ResultsComponent },
  { path: 'testdetails', component: TestdetailsComponent},
  { path: 'admin', component: AdminComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'adminaccount', component: AdminAccountComponent },
  { path: 'resetpassword', component: ResetpasswordComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled', useHash: false})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
