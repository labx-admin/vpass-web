import { Injectable } from '@angular/core';

export interface UserDetails {
    first_name: string;
    last_name: string;
    email: string;
    role: string;
    mobile: string;
    username: string;
    id: number;
    guid: string;
    birthday: string;
}

@Injectable({
    providedIn: 'root'
})

export class DataService {
    constructor() {}

    public token;
    public qrcode = null;
    public userDetails: UserDetails = {
        first_name: null,
        last_name: null,
        email: null,
        role: null,
        mobile: null,
        username: null,
        id: null,
        guid: null,
        birthday: null
    };

    getToken() {
        return localStorage.getItem('token');
    }

    setToken(value) {
        localStorage.setItem('token', value);
    }

    removeToken() {
        localStorage.removeItem('token');
    }

    getQr() {
        return localStorage.getItem('qrcode');
    }

    setQr(value) {
        localStorage.setItem('qrcode', value);
    }

    getDetails() {
        this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
        return this.userDetails;
    }

    setDetails(user: UserDetails) {
        this.userDetails.first_name = user.first_name;
        this.userDetails.last_name = user.last_name;
        this.userDetails.email = user.email;
        this.userDetails.mobile = user.mobile;
        this.userDetails.role = user.role;
        this.userDetails.username = user.username;
        this.userDetails.id = user.id;
        this.userDetails.guid = user.guid;
        this.userDetails.birthday = user.birthday;
        localStorage.setItem('userDetails', JSON.stringify(this.userDetails));
    }

    checkDetailsComplete() {
        this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
        if (!this.userDetails.first_name || !this.userDetails.last_name || !this.userDetails.email ||
            !this.userDetails.mobile || !this.userDetails.role || !this.userDetails.username ||
            !this.userDetails.id) {
                return false;
        } else {
            return true;
        }
    }
}
