import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

export interface LoginToken {
    guid: string;
    readonly token: string;
    user: {
        id: number;
        city_address: string;
        email: string;
        contact_number: string;
        firstname: string;
        lastname: string;
        role: string;
        username: string;
        birthday: string;
    };
}

export interface UserResult {
    email: string;
    contact_number: string;
    firstname: string;
    lastname: string;
    guid: string;
    birthday: string;
}

export interface VirusData {
    virustypeid: number;
    virus: string;
    tests: [{
        testtypeid: number;
        testName: string;
        results: [{
            resulttypeid: number;
            result: string;
            is_safe: boolean;
        }]
    }];
}

export interface PageResults {
    totalPages: number;
    currentPage: number;
    results: [UserResult];
}

export interface NewUser {
    first_name: string;
    last_name: string;
    id_number: string;
    id_file: File;
    mobile_number: string;
    email_id: string;
    password: string;
    dob: string;
    idtype: string;
}

export interface TestResult {
    guid: string;
    virus: string;
    test: string;
    result: string;
    is_safe: number;
    date_recorded: string;
    remarks: string;
    isDownloadable: boolean;
}

@Injectable({
    providedIn: 'root'
})

export class ApiService {
    constructor(public http: HttpClient) {}
    // Staging Endpoint
    // endpoint = 'https://staging.api.matrix-ipass.ai';

    // Prod Endpoint
    endpoint = 'https://api.vpassph.com';

    // Local Endpoint
    // endpoint = 'http://localhost:3000';

    clientId = 'TERRe7845';
    clientScrt = 'password7854';
    // Temp Base64 img
    tempImg = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAR4AAAErCAIAAACH
    HHufAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAA
    AooSURBVHhe7dztgdq4AobR1EVB1EM1NEMxew34TozkTzxvAHHOr2yQLdvoGcME9s9/QIC0IE
    JaECEtiJAWREgLIqQFEdKCCGlBhLQgQloQIS2IkBZESAsipAUR0oIIaUGEtCBCWhAhLYiQFkR
    ICyKkBRHSgghpQYS0IEJaECEtiJAWREgLIqQFEdKCCGlBhLQg4jVpnY9/CofTpX9sWr3V8dw/
    BO/mRXety+nQ1/FjKRNh8VFe9oJwY1z1cGHx1l74XquuZfJV4Yah8B5e+muMtcVUrwWFxdt7a
    Vrr3kBVAQqLD/DitJbj8iaLz/TytBZe7lUPrgvrcj4dj4dhlIfD4Xg6nWfvd8Vk43fHsvXigB
    73cXvwch4cSncU8wdBI94grZm4qlvWcljXddwPHjezthNp1XfdNWfB53uLtEbiuq6+zWEtZvV
    jvJrfT6u7V/Z/GlDWV3iTtOqM/hyPRW7jK/2vKs95I7v7/bRGLJ0GjXiXtMbierA9rIdXft2b
    r2r31S7/RVrK+hbvk9ZsXEsLstp0dIPq9eJ8Fr+WVveOq3+U7/FOaU3HtfTupNxussT5gZm0l
    g6eNr1XWlM/9PsHpyys9qHZoYm0Jiunce+WVr02V/zQX5VEryjjYWwirRWHT5PeL625tT9uYb
    E/mhscSGvF4dOmFtIql7O0eAMtpjW7ydzupcXvaSKthdU+NDt0VRbFIGkxrom0quU+uVFZ1uP
    AhWpuyj1Ii3FtpFUv+NHNlv7JeCmbkRHSYkIjadV3nG7LzR90GtvJ331MfPhXWoxqJq2xLmZV
    d6Srjfu4khajGkqr2nTG5F5X7KL4ooi0GNVUWldrvgo5v8e5HRy6kIoDlBajmkvrrntrdTo+3
    F5uX+A/X1bu7Po/ABhs/bCxtFjj/dKCJkgLIqQFEdKCCGlBhLQgQloQIS2IkBZESAsipAUR0o
    IIaUGEtCBCWhAhLYiQFkRICyKkBRHSgghpQYS0IEJaECEtiJAWREgLIqQFEdKCCGlBhLQgQlo
    QIS2IkBZESAsipAUR0oIIaUGEtCBCWhAhLYiQFkRICyKkBRHSgghpQYS0IEJaECEtiJAWREgL
    IqQFEdKCCGlBhLQgQloQIS2IkBZESAsipAUR0oIIaUHEC9M6H/8MHc/9398UD046dI7H0/nSb
    /ekLdPd55ua8HI69GPvHk/rN13Op+5A+v/g7Xx6WgOH4/OBPTXdaWy6f5LWtarbNLlw2auhtK
    6ezev56fod/Ein9VPVjbTeV2NpXT2z3J6frpwtm9a/e7nJXh+S1mHkxdel8/Aj/MfY6Hnrphu
    f7V8ub2l9jg9O60e53jpbl9z66X5hsh2k9TlaSKtTDN9849pV8sa59pDW52gkrb3rfdN0VcfD
    Q9+y+K+vMI+Hh/HX3+2P/mq/mrT0M9Gqc1k4zMd93B68nAeHejvI+9Ch20vm4Y4nT+cLtJJWv
    fY2tbVpuirj4aGvTOsy+rZtoDyE8vQqPxOtOpeNaVWn3HncZOGE9vzLyIdqJ63ZBb9k03TF4M
    eZVqTV/fzvH1ww3HZxm5/Bq85lU1rdvaf/08Cmg7tZ8SS2pJ20qud3yzO5Zbqq4YfRi2lVm8/
    5u+vF1RtMa8Rgr1tOqLoaDWs4rS3P4/rp6oX0OHhhzY5t/vNSqXurUq3Sx+0X9n6z6lwWdlRd
    y8LfnVYndH3p9/eEyleJa57IRjSUVvUkj627CcvTTf4rWjF2fs2WS7aeaP4sFoq4WXXpFnZUH
    ufV6LulFRduxSE3SVo3Y0tplerAZlfSqrO6Drp9Bvh0rn639rq0xi9mMW7HoPZI66ZeSquM/C
    CfW7Nrwpj3qrQmrn+xl12jmuO91k217aKJD77PrtknTqrwqrQmLuX2y3a14Xn5ZA2ntWXlrls
    j1y9r3V+pTe+5ybSmjnTdZStJK614XvamVS6WTc/g9ummSGvRlifmg7WTVvk0b1q426eb8s1p
    7bhsDWomrWKDjU/z762RuTW7JozO9WAmfkX4a2mVl+u5tIrD2XHZGtRIWuWS2/osb5xuxuziX
    1jQd+Wg4cE8kdbomOp6FYPWXo/fu27taSKtaqGMr9kZv7dE5hd/Mc/YRPNDZi/a3VI2Y9erHL
    P6eqw6ocPtE/DH20346Qv7cT46rZUfkFhh9VJaNJ/WyKoefDDoMvLx8fk13z1837a7Ej+HXI6
    5ns/Po/Vnj26eTWvihPoHx2bbcW0/y4ektUWxSlZZv5SWLKQ1thZnVAcyvfVg6BMX7+m0Ns72
    NWE1l9azXwvaspTmLabVWXty27YdjF6Rb/FFkR1pbfhp8UVhNZXWnm/bbVtKc9ak1Vn6KuTUh
    z06U5emvIDT+++uVHmcu9LqTLzOHNjz9HykT0+rf3+8993xP0/rZvoL/P2AKeUX5Se2Kob1o+
    7Dfjmtm/tb32dOqEUvTAtaJi2IkBZESAsipAUR0oIIaUGEtCBCWhAhLYiQFkRICyKkBRHSggh
    pQYS0IEJaECEtiJAWREgLIqQFEdKCCGlBhLQgQloQIS2IkBZESAsipAUR0oIIaUGEtCBCWhAh
    LYiQFkRICyKkBRHSgghpQYS0IEJaECEtiJAWREgLIqQFEdKCCGlBhLQgQloQIS2IkBZESAsip
    AUR0oIIaUGEtCBCWhAhLYiQFkRICyKkBREvTet8/DN0OF36ByZcTod+6N3x3D8Ab0daECEtiJ
    AWREgLIqQFEdKCiHbTupxPx8Nw/OFwOJ7Ol+k5Hg/ntvPLebCP2+b3oUPbJ+IbNJnWpVvr/ZA
    xh+NYIXVa5XRXj1M+ORHfoL20ip1OGJvrccvu3tP/aWA44/MT8Q0aS2vsRjOl2nqxlcEB7pqI
    b/BOaW1WrtlqvV9fkf2/huvbpv6ve2XKS4fzd/zOifgGLaW1fBOcv+2NHc7ou6W9E/ENGkqr2
    Nv4ap4bVB/O9n38WDWIhrWTVnGjmHgRNjeqPJwndvHXulG0q51fYzzX6XAX5R4m7jT7J+IbSK
    vfvLPycKTFGtLqN+9Ii1/UbFqLO6s9l9YTE/EN2kmreDCX1v6J+AbtpLX/brJ2B7sn4hs0lFa
    5u7H9XT8ocf1g+vF0Pl/Kz6avPpy9E/ENWkqrerzb5eDTFPUHkIoZ1x/Ozon4Bk2lVd9PZpXz
    bTmcXRPxDRpLa+yGMqGebdvh7JiIb9BcWp2RF2SF8e8obj2cpyfiG7SY1s3l/r36hw36b9b3I
    yqb07p5YiK+wUvTgnZJCyKkBRHSgghpQYS0IEJaECEtiJAWREgLIqQFEdKCCGlBhLQgQloQIS
    2IkBZESAsipAUR0oIIaUGEtCBCWhAhLYiQFkRICyKkBRHSgghpQYS0IEJaECEtiJAWREgLIqQ
    FEdKCCGlBhLQgQloQIS2IkBZESAsipAUR0oIIaUGEtCBCWhAhLYiQFkRICyKkBRHSgghpQYS0
    IEJaECEtiJAWREgLIqQFEdKCCGlBhLQg4L///geEOH0CiBmCzwAAAABJRU5ErkJggg==`;

    public login(login: string, password: string): Observable<LoginToken> {
        return this.http.post<LoginToken>(this.endpoint + '/user/login', {
            username: login,
            password,
            device_token: 'web'
        });
    }

    public loginAdmin(login: string, password: string): Observable<LoginToken> {
        return this.http.post<LoginToken>(this.endpoint + '/facilitator/login', {
            username: login,
            password,
            device_token: 'web'
        });
    }

    public resetPassword(email: string, password: string, otp: string) {
        return this.http.post(this.endpoint + '/user/resetpassword', {
            email,
            password,
            otp
        });
    }

    public forgotPassword(email: string) {
        // const headers = new HttpHeaders({
        //     client_id: this.clientId,
        //     client_secret: this.clientScrt
        // });
        // const reqOptions = {
        //     headers
        // };
        // return this.http.post(this.endpoint + '/forgotpassword', {
        //     email_id_mob_no: email
        // }, reqOptions);
        return this.http.get(this.endpoint + '/user/resetrequest/' + email);
    }

    public getQRCode(token: string): Observable<Blob> {
        const headers = new HttpHeaders({
            Authorization: 'Bearer ' + token
        });
        const reqOptions = {
            headers,
            responseType: 'blob' as 'json'
        };
        return this.http.post<Blob>(this.endpoint + '/requestqrfile', {}, reqOptions);
    }

    public registerUser(user: NewUser) {
        const headers = new HttpHeaders();
        headers.append('Content-Type', undefined);
        const reqOptions = {
            headers
        };
        const formData: FormData = new FormData();
        const tempFile = this.DataURIToBlob(this.tempImg);
        formData.append('first_name', user.first_name);
        formData.append('last_name', user.last_name);
        formData.append('idnumber', user.id_number);
        // formData.append('idCard', user.id_file, user.id_file.name);
        formData.append('mobile', user.mobile_number);
        formData.append('email_id', user.email_id);
        formData.append('password', user.password);
        formData.append('dob', user.dob);
        formData.append('gender', 'male');
        formData.append('idtype', user.idtype);
        formData.append('idCard', tempFile, 'temporary image');
        return this.http.post(this.endpoint + '/registeruser', formData, reqOptions);
    }

    public downloadPDF(testguid: string, token: string): Observable<Blob> {
        const headers = new HttpHeaders({
            Authorization: 'Bearer ' + token
        });
        const reqOptions = {
            headers,
            responseType: 'blob' as 'json'
        };
        return this.http.get<Blob>(this.endpoint + '/user/download/' + testguid, reqOptions);
    }

    public adminDownload(testguid: string, token: string): Observable<Blob> {
        const headers = new HttpHeaders({
            Authorization: 'Bearer ' + token
        });
        const reqOptions = {
            headers,
            responseType: 'blob' as 'json'
        };
        return this.http.get<Blob>(this.endpoint + '/facilitator/downloadpdf/' + testguid, reqOptions);
    }

    public register(email: string, mobile: string, password: string, firstname: string, lastname: string) {
        return this.http.post(this.endpoint + '/user/register', {
            username: email,
            email,
            password,
            firstname,
            lastname,
            contact_number: mobile
        });
    }

    public getVirusData(token: string): Observable<VirusData[]> {
        const headers = new HttpHeaders({
            Authorization: 'Bearer ' + token
        });
        const reqOptions = {
            headers
        };
        return this.http.get<VirusData[]>(this.endpoint + '/facilitator/data', reqOptions);
    }

    public addTest(
        virusid: number,
        testid: number,
        resultid: number,
        datetime: string,
        guid: string,
        isDownloadable: boolean,
        authToken: string) {
        const headers = new HttpHeaders({
            Authorization: 'Bearer ' + authToken
        });
        const reqOptions = {
            headers
        };
        return this.http.post(this.endpoint + '/facilitator/addtest', {
            virusttypeid: virusid,
            testtypeid: testid,
            resulttypeid: resultid,
            guid,
            dateRecorded: datetime,
            isDownloadable
        }, reqOptions);
    }

    public updateTest(
        guid: string,
        virusid: number,
        testid: number,
        resultid: number,
        datetime: string,
        isDownloadable: boolean,
        authToken: string) {
        const headers = new HttpHeaders({
            Authorization: 'Bearer ' + authToken
        });
        const reqOptions = {
            headers
        };
        return this.http.put(this.endpoint + '/facilitator/updateresult', {
            virusttypeid: virusid,
            testtypeid: testid,
            resulttypeid: resultid,
            guid,
            isDownloadable,
            date_recorded: datetime
        }, reqOptions);
    }

    public deleteTest(guid: string, authToken: string) {
        const headers = new HttpHeaders({
            Authorization: 'Bearer ' + authToken
        });
        const reqOptions = {
            headers
        };
        return this.http.delete(this.endpoint + '/facilitator/deletetest/' + guid, reqOptions);
    }

    public deleteUser(id: string, authToken: string) {
        const headers = new HttpHeaders({
            Authorization: 'Bearer ' + authToken
        });
        const reqOptions = {
            headers
        };
        return this.http.delete(this.endpoint + '/user/delete/' + id, reqOptions);
    }

    public updateUserDetails(guid: string, firstname: string, lastname: string, email: string, contact: string, valid_id: string, prc_number: string, birthday: string, isAdmin: boolean, authToken: string) {
        const headers = new HttpHeaders({
            Authorization: 'Bearer ' + authToken
        });
        const reqOptions = {
            headers
        };
        return this.http.put(this.endpoint + '/user/edit', {
            guid,
            firstname,
            lastname,
            email,
            contact,
            valid_id,
            prc_number,
            birthday,
            isAdmin
        }, reqOptions);
    }

    public getLatestUsers(authToken: string): Observable<UserResult[]> {
        const headers = new HttpHeaders({
            Authorization: 'Bearer ' + authToken
        });
        const reqOptions = {
            headers
        };
        return this.http.get<UserResult[]>(this.endpoint + '/facilitator/latestusers', reqOptions);
    }

    public search(query: string, page: number, authToken: string): Observable<PageResults> {
        const headers = new HttpHeaders({
            Authorization: 'Bearer ' + authToken
        });
        const reqOptions = {
            headers
        };
        return this.http.post<PageResults>(this.endpoint + '/facilitator/users/search', {
            page,
            query
        }, reqOptions);
    }

    public searchUser(query: string, authToken: string): Observable<UserResult[]> {
        const headers = new HttpHeaders({
            Authorization: 'Bearer ' + authToken
        });
        const reqOptions = {
            headers
        };
        return this.http.get<UserResult[]>(this.endpoint + '/facilitator/user/search/' + query, reqOptions);
    }

    public submitOTP(login: string, pass: string, otp: number, otpToken: string) {
        const headers = new HttpHeaders({
            Authorization: 'Bearer ' + otpToken
        });
        const reqOptions = {
            headers
        };
        return this.http.post(this.endpoint + '/submitotp', {
            email_id_mob_no: login,
            password: pass,
            otp: Number(otp),
            device_token: 'web'
        }, reqOptions);
    }

    public getUserDetails(idNum: string, idType: string, authToken: string): Observable<NewUser> {
        const headers = new HttpHeaders({
            Authorization: 'Bearer ' + authToken
        });
        const reqOptions = {
            headers
        };
        return this.http.post<NewUser>(this.endpoint + '/requestuser', {
            ID_PP_ID: idNum,
            ID_TYPE: idType
        }, reqOptions);
    }

    public getTestResults(authToken: string): Observable<TestResult[]> {
        const headers = new HttpHeaders({
            Authorization: 'Bearer ' + authToken
        });
        const reqOptions = {
            headers
        };
        return this.http.get<TestResult[]>(this.endpoint + '/user/results', reqOptions);
    }

    public getUserTestResults(guid: string): Observable<TestResult[]> {
        return this.http.get<TestResult[]>(this.endpoint + '/webresult/' + guid);
    }

    public getTestByGuid(testguid: string): Observable<TestResult> {
        return this.http.get<TestResult>(this.endpoint + '/testdetail/' + testguid);
    }

    DataURIToBlob(dataURI: string) {
        const splitDataURI = dataURI.split(',');
        const byteString = splitDataURI[0].indexOf('base64') >= 0 ? atob(splitDataURI[1]) : decodeURI(splitDataURI[1]);
        const mimeString = splitDataURI[0].split(':')[1].split(';')[0];

        const ia = new Uint8Array(byteString.length);
        for (let i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
        return new Blob([ia], { type: mimeString });
    }
}
