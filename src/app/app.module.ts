import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingPageComponent } from './modules/landing-page/landing-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SignupComponent } from './modules/signup/signup.component';
import { MainComponent } from './modules/main/main.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NotificationsComponent } from './modules/notifications/notifications.component';
import { AccountComponent } from './modules/account/account.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SetpasswordComponent } from './modules/setpassword/setpassword.component';
import { TestresultsComponent } from './modules/testresults/testresults.component';
import { NgOtpInputModule } from 'ng-otp-input';
import { HttpClientModule } from '@angular/common/http';
import { MatSelectModule, MatSlideToggleModule } from '@angular/material';
import { PopupComponent } from './modules/popup/popup.component';
import { PopupService } from './modules/popup/popup.service';
import { TermsComponent } from './modules/terms/terms.component';
import { PrivacyComponent } from './modules/privacy/privacy.component';
import { DownloadComponent } from './modules/download/download.component';
import { ResultsComponent } from './modules/results/results.component';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { AdminComponent } from './modules/admin/admin.component';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import { AdminAccountComponent } from './modules/admin-account/admin-account.component';
import { TestdetailsComponent } from './modules/testdetails/testdetails.component';
import { ResetpasswordComponent } from './modules/resetpassword/resetpassword.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    SignupComponent,
    MainComponent,
    NotificationsComponent,
    AccountComponent,
    SetpasswordComponent,
    TestresultsComponent,
    PopupComponent,
    TermsComponent,
    PrivacyComponent,
    DownloadComponent,
    ResultsComponent,
    AdminComponent,
    DashboardComponent,
    AdminAccountComponent,
    TestdetailsComponent,
    ResetpasswordComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    NgOtpInputModule,
    HttpClientModule,
    NgxQRCodeModule,
    MatSlideToggleModule
  ],
  providers: [
    MatDatepickerModule,
    PopupService,
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
