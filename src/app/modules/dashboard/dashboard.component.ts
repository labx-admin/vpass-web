import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { DataService, UserDetails } from '../../services/data.service';
import { ApiService } from '../../services/api.service';
import { faEdit, faTimes, faDownload, faSearch } from '@fortawesome/free-solid-svg-icons';
import * as moment from 'moment';
import * as FileSaver from 'file-saver';
import { PopupService } from '../popup/popup.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent implements OnInit {

  constructor(private apiService: ApiService,
              private dataService: DataService,
              private route: Router,
              private popupService: PopupService) { }
  edit = faEdit;
  close = faTimes;
  download = faDownload;
  searchIcon = faSearch;
  token = '';
  users = [];
  virusdata = [];
  testdata = [];
  resultdata = [];
  active = {
    firstname: '',
    lastname: '',
    contact_number: '',
    email: '',
    guid: '',
    valid_id: '',
    prc_number: '',
    birthday: '',
    userroleid: 0
  };
  userDetails =  {
    firstname: '',
    lastname: '',
    contact_number: '',
    email: '',
    guid: '',
    valid_id: '',
    prc_number: '',
    birthday: null
  };
  facilitatorDetails: UserDetails;
  userresults = [];
  selectedVirus = null;
  selectedTest = null;
  selectedResult = null;
  selectedDate = null;
  selectedDownload = false;
  isAdmin = false;
  activeDownload = 0;
  showSuccess = false;
  activeResults = true;
  addTest = true;
  editGuid = null;
  showModal = false;
  downloading = false;
  selectedCard = 4;
  activeIndex = -1;
  deleteUserModal = false;
  deleteTestModal = false;
  editUserModal = false;
  totalPages = 1;
  currentPage = 1;
  query = '';
  resultMessage = '';
  ngOnInit() {
    this.token = this.dataService.getToken();
    this.facilitatorDetails = this.dataService.getDetails();
    console.log(this.facilitatorDetails);
    if (!this.token) {
      this.route.navigate(['admin']);
    } else {
      this.resultMessage = 'Retrieving Users';
      this.apiService.getVirusData(this.token).subscribe(data => {
        data.forEach(item => {
          this.virusdata.push(item);
        });
        this.testdata = this.virusdata[0].tests;
        this.resultdata = this.testdata[0].results;
      });
      this.apiService.search(null, 1, this.token).subscribe(data => {
        const results = data.results;
        if (data.results.length < 1) {
          this.resultMessage = 'No users found matching search criteria';
        }
        results.forEach(item => {
          item.birthday = moment(item.birthday).format("DD/MM/YYYY");
          this.users.push(item);
        });
        this.totalPages = data.totalPages;
        console.log(data);
      });
      // this.apiService.getLatestUsers(this.token).subscribe(data => {
      //   data.forEach(item => {
      //     this.users.push(item);
      //   });
      //   console.log(this.users);
      // });
    }
  }

  editUser(index: number) {
    this.addTest = true;
    this.activeResults = true;
    const activeuser = this.users[index];
    this.active = activeuser;
    this.showSuccess = false;
    this.apiService.getUserTestResults(activeuser.guid).subscribe(data => {
      this.userresults = [];
      let i = 0;
      while (i < data.length) {
        data[i].date_recorded = moment(data[i].date_recorded).format('MMMM D, YYYY h:mm A');
        this.userresults.push(data[i]);
        i++;
      }
    });
  }

  deleteUser(index: number) {
    this.active = this.users[index];
    this.deleteUserModal = true;
  }

  confirmDeleteUser() {
    const guid = this.active.guid;
    this.apiService.deleteUser(guid, this.token).subscribe(data => {
      this.searchUser('');
      this.deleteUserModal = false;
    });
  }

  detectEmpty() {
    if (this.query.length === 0) {
      this.searchUser('');
    }
  }

  searchUser(query: string) {
    this.resultMessage = 'Retrieving Users';
    if (query.length > 0) {
      this.users = [];
      this.apiService.search(query, 1, this.token).subscribe(data => {
        data.results.forEach(item => {
          this.users.push(item);
        });
        if (data.results.length < 1) {
          this.resultMessage = 'No users found matching search criteria';
        }
        this.totalPages = data.totalPages;
        this.currentPage = 1;
      });
    } else
    if (query.length === 0) {
      this.users = [];
      this.apiService.search(null, 1, this.token).subscribe(data => {
        data.results.forEach(item => {
          this.users.push(item);
        });
        if (data.results.length < 1) {
          this.resultMessage = 'No users found matching search criteria';
        }
        this.totalPages = data.totalPages;
        this.currentPage = 1;
      });
    }
  }

  toggleModal(state: boolean, index: number) {
    this.showModal = state;
    if (this.showModal) {
      this.userresults = [
        {
          is_active: 0
        }
      ];
      this.activeIndex = index;
      const activeuser = this.users[index];
      console.log(activeuser);
      this.active = activeuser;
      this.showSuccess = false;
      this.apiService.getUserTestResults(activeuser.guid).subscribe(data => {
        this.userresults = [];
        console.log(data);
        let i = 0;
        while (i < data.length) {
          data[i].date_recorded = moment(data[i].date_recorded).format('MMMM D, YYYY h:mm A');
          this.userresults.push(data[i]);
          i++;
        }
      });
    } else {
      this.selectedCard = 4;
      this.selectedVirus = null;
      this.selectedTest = null;
      this.selectedResult = null;
      this.selectedDate = moment().format('YYYY-MM-DDTHH:mm');
      this.selectedDownload = false;
      this.activeDownload = 0;
    }
  }

  selectCard(index: number) {
    this.selectedCard = index;
    if (index < 4) {
      this.editResults(index);
    } else {
      this.selectedVirus = null;
      this.selectedTest = null;
      this.selectedResult = null;
      this.selectedDate = moment().format('YYYY-MM-DDTHH:mm');
      this.selectedDownload = false;
      this.activeDownload = 0;
    }
  }

  changeVirus(index) {
    this.testdata = [];
    this.resultdata = [];
    this.testdata = this.virusdata[this.virusdata.findIndex(obj => obj.virusttypeid === parseInt(index, 10))].tests;
    this.resultdata = this.testdata[0].results;
    this.selectedTest = this.testdata[0].testtypeid;
    this.selectedResult = this.resultdata[0].resulttypeid;
  }

  changeTest(index) {
    this.resultdata = [];
    this.resultdata = this.testdata[this.testdata.findIndex(obj => obj.testtypeid === parseInt(index, 10))].results;
    this.selectedResult = this.resultdata[0].resulttypeid;
  }

  toggleAddNew() {
    this.activeResults = false;
    this.selectedVirus = null;
    this.selectedTest = null;
    this.selectedResult = null;
    this.selectedDownload = false;
    this.selectedDate = moment().format('YYYY-MM-DDTHH:mm');
  }

  editResults(index: number) {
    const userResultData = this.userresults[index];
    const virusIndex = this.virusdata.findIndex(obj => obj.virus === userResultData.virus);
    const virus = this.virusdata[virusIndex];
    this.testdata = virus.tests;
    this.selectedVirus = virus.virusttypeid;
    const test = virus.tests[virus.tests.findIndex(obj => obj.testName === userResultData.test)];
    this.selectedTest = test.testtypeid;
    const testres = test.results[test.results.findIndex(obj => obj.result === userResultData.result)];
    this.resultdata = test.results;
    this.selectedResult = testres.resulttypeid;
    const dateTime = moment(userResultData.date_recorded).format('YYYY-MM-DDTHH:mm');
    this.selectedDate = dateTime;
    this.activeResults = false;
    this.addTest = false;
    this.editGuid = userResultData.guid;
    this.selectedDownload = userResultData.isDownloadable;
    this.activeDownload = userResultData.isDownloadable;
  }

  addResult() {
    if (this.selectedVirus && this.selectedTest && this.selectedResult && this.selectedDate) {
      this.apiService.addTest(this.selectedVirus, this.selectedTest, this.selectedResult,
        this.selectedDate, this.active.guid, this.selectedDownload, this.token).subscribe(data => {
        this.selectedVirus = null;
        this.selectedTest = null;
        this.selectedResult = null;
        this.selectedDate = null;
        this.selectedDownload = false;
        this.showSuccess = true;
        this.selectedCard = 4;
        this.toggleModal(true, this.activeIndex);
      });
    }
  }

  editResult() {
    if (this.selectedVirus && this.selectedTest && this.selectedResult && this.selectedDate) {
      this.apiService.updateTest(this.editGuid, this.selectedVirus, this.selectedTest,
        this.selectedResult, this.selectedDate, this.selectedDownload, this.token).subscribe(data => {
        this.selectedVirus = null;
        this.selectedTest = null;
        this.selectedResult = null;
        this.selectedDate = null;
        this.selectedDownload = false;
        this.showSuccess = true;
        this.selectedCard = 4;
        this.toggleModal(true, this.activeIndex);
      });
    }
  }

  changePage(page: number) {
    let currQuery = this.query;
    if (this.query.length < 1) {
      currQuery = null;
    }
    this.resultMessage = 'Retrieving Users';
    this.apiService.search(currQuery, page, this.token).subscribe(data => {
      this.totalPages = data.totalPages;
      this.currentPage = page;
      this.users = [];
      if (data.results.length < 1) {
        this.resultMessage = 'No users found matching search criteria';
      }
      data.results.forEach(item => {
        this.users.push(item);
      });
    });
  }

  openUserDetails() {
    this.editUserModal = true;
    this.userDetails.firstname = this.active.firstname;
    this.userDetails.lastname = this.active.lastname;
    this.userDetails.email = this.active.email;
    this.userDetails.contact_number = this.active.contact_number;
    this.userDetails.guid = this.active.guid;
    this.userDetails.valid_id = this.active.valid_id;
    this.userDetails.prc_number = this.active.prc_number;
    console.log(this.active.birthday);
    this.userDetails.birthday = new Date(moment(this.active.birthday).format("YYYY-DD-MM"));
    this.isAdmin = this.active.userroleid === 2 || this.active.userroleid === 4 ? true : false;
    console.log(this.userDetails);
  }

  editUserDetails() {
    this.apiService.updateUserDetails(
      this.userDetails.guid,
      this.userDetails.firstname,
      this.userDetails.lastname,
      this.userDetails.email,
      this.userDetails.contact_number,
      this.userDetails.valid_id,
      this.userDetails.prc_number,
      moment(this.userDetails.birthday).format("YYYY-MM-DD"),
      this.isAdmin,
      this.token).subscribe(data => {
        this.users = [];
        this.resultMessage = 'Retrieving Users';
        this.apiService.search(this.query, this.currentPage, this.token).subscribe(searchdata => {
          searchdata.results.forEach(item => {
            item.birthday = moment(item.birthday).format("DD/MM/YYYY");
            this.users.push(item);
          });
          if (searchdata.results.length < 1) {
            this.resultMessage = 'No users found matching search criteria';
          }
          this.editUserModal = false;
          this.toggleModal(true, this.activeIndex);
        });
      });
  }

  deleteResult() {
    this.apiService.deleteTest(this.editGuid, this.token).subscribe(data => {
      this.selectedVirus = null;
      this.selectedTest = null;
      this.selectedResult = null;
      this.selectedDate = null;
      this.selectedDownload = false;
      this.showSuccess = true;
      this.selectedCard = 4;
      this.toggleModal(true, this.activeIndex);
      this.deleteTestModal = false;
    });
  }
  OnChangeVirus(newValue) {
    console.log(newValue);
  }

  showValue(value: string) {
    console.log(value);
  }

  downloadFile(guid: string) {
    this.downloading = true;
    const token = this.dataService.getToken();
    this.apiService.adminDownload(guid, token).subscribe(data => {
      const blob = new Blob([data], {type: 'application/pdf'});
      FileSaver.saveAs(blob, 'Certificate of Testing_' + this.active.firstname + '_' + this.active.lastname + '.pdf');
      this.downloading = false;
    });
  }

  dateChange(eventDate: Date) {
    this.userDetails.birthday = eventDate;
  }
}
