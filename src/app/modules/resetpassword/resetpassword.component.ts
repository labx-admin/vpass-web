import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { PopupService } from '../popup/popup.service';

export function checkSamePassword(c: AbstractControl) {
  if (c && c.get('password').value && c.get('repeatpassword').value) {
    if (c.get('password').value === c.get('repeatpassword').value) {
      return null;
    }
  }
  return { matching: false };
}

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ResetpasswordComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private apiService: ApiService,
    private popupService: PopupService
  ) {
    const param1 = 'email';
    const param2 = 'otp';
    this.resetForm = this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(8)]],
      repeatpassword: ['', [Validators.required, Validators.minLength(8)]]
    }, { validator: checkSamePassword });
    this.route.queryParams.subscribe(params => {
      this.email = params[param1];
      this.otp = params[param2];
    });
  }
  email = '';
  otp = '';
  response = 0;
  resetForm: FormGroup;
  hide = true;
  hideFields = false;

  ngOnInit() {
  }

  togglePasswordVisible() {
    this.hide = !this.hide;
  }

  checkMatching() {
    const password = this.resetForm.get('password').value;
    const repeatpassword = this.resetForm.get('repeatpassword').value;
    if (repeatpassword === password) {
      return true;
    }
    return false;
  }

  reset() {
    this.email = decodeURI(this.email);
    this.apiService.resetPassword(this.email, this.resetForm.get('password').value, this.otp).subscribe({
      next: data => {
        this.popupService.popupToggle('Password has been successfully reset');
        this.response = 1;
        this.resetForm.reset();
        this.hideFields = true;
        Object.keys(this.resetForm.controls).forEach(key => {
          this.resetForm.get(key).setErrors(null) ;
        });
      },
      error: error => {
        this.popupService.popupToggle('Error resetting password');
        this.response = 2;
      }
    });
  }
}
