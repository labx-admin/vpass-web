import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { faQrcode,
  faUserCircle,
  faSignOutAlt,
  faPencilAlt,
  faQuestionCircle,
  faMinusCircle,
  faPlusCircle,
  faSync,
  faDownload
} from '@fortawesome/free-solid-svg-icons';
import * as moment from 'moment';
import { ApiService } from '../../services/api.service.js';
import { DataService, UserDetails } from '../../services/data.service.js';
import { PopupService } from '../popup/popup.service.js';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-testresults',
  templateUrl: './testresults.component.html',
  styleUrls: ['./testresults.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class TestresultsComponent implements OnInit {
  qrIcon = faQrcode;
  download = faDownload;
  userCircle = faUserCircle;
  signOut = faSignOutAlt;
  edit = faPencilAlt;
  untested = faQuestionCircle;
  plus = faPlusCircle;
  minus = faMinusCircle;
  refresh = faSync;
  latestResult = 2;
  user: UserDetails;
  testResults = [];
  constructor(
    private apiService: ApiService,
    private dataService: DataService,
    private popupService: PopupService,
    private route: Router
  ) { }

  ngOnInit() {
    const token = this.dataService.getToken();
    if (!token) {
      this.route.navigate(['']);
    }
    this.user = this.dataService.getDetails();
    this.apiService.getTestResults(token).subscribe(data => {
      this.testResults = data;
      console.log(this.testResults);
      if (this.testResults[0]) {
        this.latestResult = this.testResults[0].is_safe;
        this.testResults.forEach(item => {
          if (item.date_recorded) {
            const date = new Date(item.date_recorded);
            item.date_recorded = moment(date).format('MMMM D, YYYY H:mm A');
          }
        });
      }
    },
    error => {
      if (error.status === 404) {
        this.testResults[0] = {
          firstname: '',
          lastname: '',
          contact_number: '',
          test: '',
          date_recorded: ''
        };
      }
    });
  }

  refreshResults() {
    const token = this.dataService.getToken();
    this.apiService.getTestResults(token).subscribe(data => {
      this.testResults = data;
      if (this.testResults[0]) {
        this.latestResult = this.testResults[0].is_safe;
      }
      this.testResults.forEach(item => {
        if (item.date_recorded) {
          const date = new Date(item.date_recorded);
          item.date_recorded = moment(date).format('MMMM D, YYYY H:mm A');
        }
      });
    },
    error => {
      if (error.status === 401) {
        this.dataService.removeToken();
        this.popupService.popupToggle('Session Expired. Please login again.');
        this.route.navigate(['']);
      }
    });
  }

  logout() {
    this.dataService.removeToken();
    this.route.navigate(['']);
  }

  downloadFile(guid: string) {
    const token = this.dataService.getToken();
    this.apiService.downloadPDF(guid, token).subscribe(data => {
      const blob = new Blob([data], {type: 'application/pdf'});
      FileSaver.saveAs(blob, 'Certificate of Testing_' + this.user.first_name + '_' + this.user.last_name + '.pdf');
    });
  }

}
