import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-admin-account',
  templateUrl: './admin-account.component.html',
  styleUrls: ['./admin-account.component.scss']
})
export class AdminAccountComponent implements OnInit {

  constructor(private dataService: DataService, private route: Router) { }

  ngOnInit() {
  }

  logout() {
    this.dataService.removeToken();
    this.route.navigate(['admin']);
  }
}
