import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { faQrcode, faUserCircle, faSignOutAlt, faPencilAlt, faMinusCircle, faSync } from '@fortawesome/free-solid-svg-icons';
import { DataService, UserDetails } from '../../services/data.service.js';
import { ApiService } from '../../services/api.service.js';
import { PopupService } from '../popup/popup.service.js';
import * as moment from 'moment';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AccountComponent implements OnInit {
  qrIcon = faQrcode;
  userCircle = faUserCircle;
  signOut = faSignOutAlt;
  edit = faPencilAlt;
  minus = faMinusCircle;
  refresh = faSync;
  editForm = false;
  accountForm: FormGroup;
  user: UserDetails;
  hide = true;
  constructor(
    private formBuilder: FormBuilder,
    private dataService: DataService,
    private apiService: ApiService,
    private popupService: PopupService,
    private route: Router
  ) { }

  ngOnInit() {
    const token = this.dataService.getToken();
    if (!token) {
      this.route.navigate(['']);
    }
    this.user = this.dataService.getDetails();
    this.user.birthday = moment(this.user.birthday).format("MMMM Do YYYY");
    // if (!this.dataService.checkDetailsComplete()) {
    //   this.apiService.getUserDetails(this.user.idNum, this.user.idType, token).subscribe(data => {
    //     this.user.email_id = data.email_id;
    //     this.user.dob = data.dob;
    //   },
    //   error => {
    //     if (error.status === 401) {
    //       this.dataService.removeToken();
    //       this.popupService.popupToggle('Session Expired. Please login again.');
    //       this.route.navigate(['']);
    //     }
    //   });
    // }
    this.accountForm = this.formBuilder.group({
      mobileNumber: [{value: null, disabled: true}, [Validators.required]],
      password: [{value: null, disabled: true}, [Validators.required]]
    });
  }
  // toggleEdit() {
  //   this.editForm = !this.editForm;
  //   if (this.editForm) {
  //     this.accountForm.controls['mobileNumber'].enable();
  //     this.accountForm.controls['password'].enable();
  //   }
  //   else {
  //     this.accountForm.controls['mobileNumber'].disable();
  //     this.accountForm.controls['password'].disable();
  //   }
  // }
  togglePasswordVisible() {
    this.hide = !this.hide;
  }
  logout() {
    this.dataService.removeToken();
    this.route.navigate(['']);
  }
}
