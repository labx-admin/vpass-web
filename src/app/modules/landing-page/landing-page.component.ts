import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Router } from '@angular/router';
import { ApiService } from '../../services/api.service.js';
import { DataService } from '../../services/data.service.js';
import { PopupService } from '../popup/popup.service.js';

export class LoginErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LandingPageComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private loginService: ApiService,
    private route: Router,
    private popupService: PopupService,
    private dataService: DataService) { }
  loginForm: FormGroup;
  forgotForm: FormGroup;
  active = 'default';
  hide = true;
  loginFormControl = new FormControl('', [
    Validators.required
  ]);

  matcher = new LoginErrorStateMatcher();
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      login: [null, [Validators.required]],
      password: [null, [Validators.required]]
    });
    this.forgotForm = this.formBuilder.group({
      email: [null, [Validators.required, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')]]
    });
  }

  togglePasswordVisible() {
    this.hide = !this.hide;
  }

  changePanel(panel: string) {
    this.active = panel;
  }

  login() {
    if (!this.loginForm.valid) {
      return;
    } else {
      const loginKey = 'login';
      const passwordKey = 'password';
      this.loginService.login(this.loginForm.controls[loginKey].value, this.loginForm.controls[passwordKey].value).subscribe({
        next: data => {
          this.dataService.setToken(data.token);
          const user = {
            first_name: data.user.firstname,
            last_name: data.user.lastname,
            email: data.user.email,
            mobile: data.user.contact_number,
            role: data.user.role,
            username: data.user.username,
            id: data.user.id,
            guid: data.guid,
            birthday: data.user.birthday
          };
          this.dataService.setDetails(user);
          this.route.navigate(['main']);
        },
        error: error => {
          if (error.status === 400) {
            this.popupService.popupToggle('Invalid username or password');
          }
        }
      });
    }
  }
  resetRequest() {
    if (!this.forgotForm.valid) {
      return;
    } else {
      const index = 'email';
      this.loginService.forgotPassword(this.forgotForm.controls[index].value).subscribe({
        next: data => {
          this.popupService.popupToggle('Password reset request has been submitted');
          setTimeout(() => {
            window.location.reload();
          }, 1000);
        },
        error: error => {
          this.popupService.popupToggle('Error sending password reset request');
        }
      });
    }
  }
}
