import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-setpassword',
  templateUrl: './setpassword.component.html',
  styleUrls: ['./setpassword.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SetpasswordComponent implements OnInit {

  constructor(private fromBuilder: FormBuilder) { }
  hide = true;
  passwordForm: FormGroup;
  ngOnInit() {
    this.passwordForm = this.fromBuilder.group({
      password: [null, [Validators.required]]
    });
  }

  togglePasswordVisible() {
    this.hide = !this.hide;
  }

  setPassword() {
    if (!this.passwordForm.valid) {
      // console.log(this.passwordForm.value);
      return;
    } else {
      // console.log(this.passwordForm.value);
    }
  }
}
