import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { faQrcode,
  faUserCircle,
  faSignOutAlt,
  faPencilAlt,
  faQuestionCircle,
  faMinusCircle,
  faPlusCircle,
  faSync
} from '@fortawesome/free-solid-svg-icons';
import * as moment from 'moment';
import { ApiService } from '../../services/api.service.js';
import { DataService, UserDetails } from '../../services/data.service.js';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {
  qrIcon = faQrcode;
  userCircle = faUserCircle;
  signOut = faSignOutAlt;
  edit = faPencilAlt;
  untested = faQuestionCircle;
  plus = faPlusCircle;
  minus = faMinusCircle;
  refresh = faSync;
  latestResult = 2;
  user: UserDetails;
  testResults = [];
  guid = '';
  constructor(private router: ActivatedRoute, private apiService: ApiService) {
  }

  ngOnInit() {
    this.guid = this.router.snapshot.queryParams.guid;
    console.log(this.guid);
    this.apiService.getUserTestResults(this.guid).subscribe(data => {
      this.testResults = data;
      console.log(data);
      if (this.testResults[0]) {
        this.latestResult = this.testResults[0].is_safe;
        this.testResults.forEach(item => {
          if (item.date_recorded) {
            const date = new Date(item.date_recorded);
            item.date_recorded = moment(date).format('MMMM D, YYYY H:mm A');
          }
        });
      }
    });
  }

}
