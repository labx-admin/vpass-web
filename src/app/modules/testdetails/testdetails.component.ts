import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { faQrcode,
  faUserCircle,
  faSignOutAlt,
  faPencilAlt,
  faQuestionCircle,
  faMinusCircle,
  faPlusCircle,
  faSync
} from '@fortawesome/free-solid-svg-icons';
import * as moment from 'moment';
import { ApiService } from '../../services/api.service.js';
import { DataService, UserDetails } from '../../services/data.service.js';

@Component({
  selector: 'app-testdetails',
  templateUrl: './testdetails.component.html',
  styleUrls: ['./testdetails.component.scss']
})
export class TestdetailsComponent implements OnInit {
  qrIcon = faQrcode;
  userCircle = faUserCircle;
  signOut = faSignOutAlt;
  edit = faPencilAlt;
  untested = faQuestionCircle;
  plus = faPlusCircle;
  minus = faMinusCircle;
  refresh = faSync;
  latestResult = 2;
  user: UserDetails;
  testResults = null;
  guid = '';
  constructor(private router: ActivatedRoute, private apiService: ApiService) { }

  ngOnInit() {
    this.guid = this.router.snapshot.queryParams.guid;
    this.apiService.getTestByGuid(this.guid).subscribe(data => {
      this.testResults = data;
      if (this.testResults.date_recorded) {
        const date = new Date(this.testResults.date_recorded);
        this.testResults.date_recorded = moment(date).format('MMMM D, YYYY H:mm A');
      }
    });
  }

}
