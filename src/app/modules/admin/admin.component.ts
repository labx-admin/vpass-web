import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Router } from '@angular/router';
import { ApiService } from '../../services/api.service.js';
import { DataService } from '../../services/data.service.js';
import { PopupService } from '../popup/popup.service.js';

export class LoginErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AdminComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private route: Router,
    private authService: ApiService,
    private dataService: DataService,
    private popupService: PopupService
  ) { }
  adminLogin: FormGroup;
  hide = true;
  adminFormControl = new FormControl('', [
    Validators.required
  ]);
  matcher = new LoginErrorStateMatcher();
  ngOnInit() {
    this.adminLogin = this.formBuilder.group({
      login: [null, [Validators.required]],
      password: [null, [Validators.required]]
    });
  }

  togglePasswordVisible() {
    this.hide = !this.hide;
  }

  login() {
    if (!this.adminLogin.valid) {
      return;
    } else {
      console.log('login toggled');
      const loginKey = 'login';
      const passwordKey = 'password';
      this.authService.loginAdmin(this.adminLogin.controls[loginKey].value, this.adminLogin.controls[passwordKey].value).subscribe({
        next: data => {
          this.dataService.setToken(data.token);
          const user = {
            first_name: data.user.firstname,
            last_name: data.user.lastname,
            email: data.user.email,
            mobile: data.user.contact_number,
            role: data.user.role,
            username: data.user.username,
            id: data.user.id,
            guid: data.guid,
            birthday: data.user.birthday
          };
          this.dataService.setDetails(user);
          this.route.navigate(['dashboard']);
        },
        error: error => {
          if (error.status === 400 || error.status === 401) {
            this.popupService.popupToggle('Invalid username or password');
          }
        }
      });
    }
  }
}
