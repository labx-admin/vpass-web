import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../services/api.service.js';
import * as moment from 'moment';
import { PopupService } from '../popup/popup.service.js';
import { PlatformLocation } from '@angular/common';
import { faTimesCircle, faArrowLeft } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SignupComponent implements OnInit, OnDestroy {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private popupService: PopupService,
    private apiService: ApiService,
    private location: PlatformLocation
  ) { }
  backArrow = faArrowLeft;
  cancelButton = faTimesCircle;
  step1 = 'regular';
  hide = true;
  loading = false;
  step = 0;
  photo = null;
  private sub: any;
  page;
  nextPage;
  valid = false;
  signupForm: FormGroup;
  buttonText = 'Next';
  stepwidth: number;
  uploadFile = null;
  otp: number = null;
  otpToken: string = null;
  signupData = {
    email: null,
    mobile: null,
    password: null,
    firstName: null,
    lastName: null,
    dob: null,
    idNum: null,
    idType: null
  };
  ngOnInit() {
    this.initialize();
    this.location.onPopState(() => {
      this.initialize();
    });
  }

  initialize() {
    this.sub = this.route.params.subscribe(params => {
      const pageKey = 'page';
      // this.page = +params[pageKey];
      this.page = parseInt(this.router.url.slice(-1), 10);
      if (this.page < 4) {
        this.nextPage = this.page + 1;
      } else {
        this.nextPage = this.page;
        this.buttonText = 'Complete sign up';
        this.otpToken = history.state.otpToken;
      }
      const state = history.state.signupData;
      this.valid = false;
      this.signupForm = this.formBuilder.group({
        email: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')]],
        mobile: ['', [Validators.required]],
        password: ['', [Validators.required, Validators.minLength(8)]],
        firstName: ['', [Validators.required]],
        lastName: ['', [Validators.required]],
        dob: [null, [Validators.required]],
        idNum: ['', [Validators.required]],
        idType: [null, [Validators.required]]
      });
      this.signupForm.patchValue({
        email: sessionStorage.getItem('email') || '',
        mobile: sessionStorage.getItem('mobile') || '',
        password: sessionStorage.getItem('password') || '',
        firstName: sessionStorage.getItem('firstName') || '',
        lastName: sessionStorage.getItem('lastName') || '',
        dob: sessionStorage.getItem('dob') || null,
        idNum: sessionStorage.getItem('idNum') || '',
        idType: sessionStorage.getItem('idType') || null
      });
      // if (state) {
      //   this.signupForm = this.formBuilder.group({
      //     email: [state.email || null, [Validators.required, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')]],
      //     mobile: [state.mobile || null, [Validators.required]],
      //     password: [state.password || null, [Validators.required]],
      //     firstName: [state.firstName || null, [Validators.required]],
      //     lastName: [state.lastName || null, [Validators.required]],
      //     dob: [state.dob || null, [Validators.required]],
      //     idNum: [state.idNum || null, [Validators.required]],
      //     idType: [state.idNum || null, [Validators.required]]
      //   });
      // } else {
      //   this.signupForm = this.formBuilder.group({
      //     email: [null, [Validators.required, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')]],
      //     mobile: [null, [Validators.required]],
      //     password: [null, [Validators.required, Validators.pattern('^(?![0-9]{6})[0-9a-zA-Z]{8,}$')]],
      //     firstName: [null, [Validators.required]],
      //     lastName: [null, [Validators.required]],
      //     dob: [null, [Validators.required]],
      //     idNum: [null, [Validators.required]],
      //     idType: [null, [Validators.required]]
      //   });
      // }
      this.stepwidth = (100 / 5) * this.page;
      this.validCheck();
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  togglePasswordVisible() {
    this.hide = !this.hide;
  }
  checked(event: any) {
    this.step1 = event.target.value;
  }
  validCheck() {
    switch (this.page) {
      case 1:
        const key1 = 'email';
        if (this.signupForm.controls[key1].value === '') {
          this.valid = false;
        } else
        if (this.signupForm.controls[key1].valid) {
          this.valid = true;
        } else {
          this.valid = false;
        }
        break;
      case 2:
        const key2 = 'mobile';
        if (this.signupForm.controls[key2].value === null) {
          this.valid = false;
        } else
        if (this.signupForm.controls[key2].valid && this.signupForm.controls[key2].value.toString().length > 10) {
          this.valid = true;
        } else {
          this.valid = false;
        }
        break;
      case 3:
        const key3 = 'password';
        if (this.signupForm.controls[key3].value === '') {
          this.valid = false;
        } else
        if (this.signupForm.controls[key3].valid) {
          this.valid = true;
        } else {
          this.valid = false;
        }
        break;
      case 4:
        const key4 = 'firstName';
        const key4b = 'lastName';
        if (this.signupForm.controls[key4].value === '') {
          this.valid = false;
        } else
        if (this.signupForm.controls[key4].valid && this.signupForm.controls[key4b].valid) {
          this.valid = true;
        } else {
          this.valid = false;
        }
        break;
      case 5:
        const key5 = 'dob';
        const date = moment(this.signupForm.controls.dob.value);
        if (this.signupForm.controls[key5].value === '' && date.isValid()) {
          this.valid = false;
        } else
        if (this.signupForm.controls[key5].valid) {
          this.valid = true;
        } else {
          this.valid = false;
        }
        break;
      case 6:
        const key6 = 'idNum';
        if (this.signupForm.controls[key6].value === ''
        // || this.uploadFile === null
        ) {
          this.valid = false;
        } else
        if (this.signupForm.controls[key6].valid
          // && this.uploadFile !== null
          ) {
          this.valid = true;
        } else {
          this.valid = false;
        }
        break;
      // case 7:
      //   const key7 = 'idNum';
      //   if (this.signupForm.controls['idNum'].value === '') { this.valid = false; }
      //   else if (this.signupForm.controls['idNum'].valid && this.signupForm.controls['idImage'].valid) { this.valid = true; }
      //   else this.valid = false;
      //   break;
      default:
        break;
    }
  }
  next() {
    if (this.page === 4 && this.valid) {
      this.apiService.register(this.signupForm.controls.email.value,
        this.signupForm.controls.mobile.value,
        this.signupForm.controls.password.value,
        this.signupForm.controls.firstName.value,
        this.signupForm.controls.lastName.value).subscribe({
          next: data => {
            sessionStorage.removeItem('email');
            sessionStorage.removeItem('mobile');
            sessionStorage.removeItem('password');
            sessionStorage.removeItem('firstName');
            sessionStorage.removeItem('lastName');
            sessionStorage.removeItem('dob');
            sessionStorage.removeItem('idNum');
            sessionStorage.removeItem('idType');
            sessionStorage.removeItem('uploadFile');
            this.popupService.popupToggle('Registration Complete. Please log in.');
            this.router.navigate(['/home']);
          },
          error: error => {
            this.popupService.popupToggle('Error Registering Account');
          }
        });
      // this.apiService.submitOTP(login, pass, this.otp, this.otpToken).subscribe({
      //   next: data => {
      //     sessionStorage.removeItem('email');
      //     sessionStorage.removeItem('mobile');
      //     sessionStorage.removeItem('password');
      //     sessionStorage.removeItem('firstName');
      //     sessionStorage.removeItem('lastName');
      //     sessionStorage.removeItem('dob');
      //     sessionStorage.removeItem('idNum');
      //     sessionStorage.removeItem('idType');
      //     sessionStorage.removeItem('uploadFile');
      //     this.popupService.popupToggle('Registration Complete. Please log in.');
      //     this.router.navigate(['/home']);
      //   },
      //   error: error => {
      //     if (error.status === 400) {
      //       this.popupService.popupToggle('Invalid OTP');
      //     }
      //   }
      // });
    } else
    if (this.page === 6 && this.valid) {
      this.loading = true;
      const newUser = {
        first_name: this.signupForm.controls.firstName.value,
        last_name: this.signupForm.controls.lastName.value,
        id_number: this.signupForm.controls.idNum.value,
        id_file: this.uploadFile,
        mobile_number: this.signupForm.controls.mobile.value,
        email_id: this.signupForm.controls.email.value,
        password: this.signupForm.controls.password.value,
        dob: this.signupForm.controls.dob.value,
        idtype: this.signupForm.controls.idType.value
      };
      this.apiService.registerUser(newUser).subscribe({
        next: data => {
          this.loading = false;
          const jwtKey = 'JWT_token_temp';
          this.router.navigate(['/signup', 7],
          {
            state: {
              otpToken: data[jwtKey],
              signupData: this.signupData,
              uploadFile: this.uploadFile
            }
          });
        },
        error: error => {
          this.popupService.popupToggle('Error encountered with registration');
        }
      });
    } else
    if (this.valid) {
      sessionStorage.setItem('email', this.signupForm.controls.email.value);
      sessionStorage.setItem('mobile', this.signupForm.controls.mobile.value.toString());
      sessionStorage.setItem('password', this.signupForm.controls.password.value);
      sessionStorage.setItem('firstName', this.signupForm.controls.firstName.value.toString().substring(0, 12));
      sessionStorage.setItem('lastName', this.signupForm.controls.lastName.value.toString().substring(0, 12));
      sessionStorage.setItem('dob', moment(this.signupForm.controls.dob.value).format('YYYY-MM-DD'));
      sessionStorage.setItem('idNum', this.signupForm.controls.idNum.value);
      sessionStorage.setItem('idType', this.signupForm.controls.idType.value);
      sessionStorage.setItem('uploadFile', this.uploadFile);
      // this.signupData.email = this.signupForm.controls.email.value;
      // this.signupData.mobile = this.signupForm.controls.mobile.value;
      // this.signupData.password = this.signupForm.controls.password.value;
      // this.signupData.firstName = this.signupForm.controls.firstName.value;
      // this.signupData.lastName = this.signupForm.controls.lastName.value;
      // this.signupData.dob = moment(this.signupForm.controls.dob.value).format('YYYY-MM-DD');
      // this.signupData.idNum = this.signupForm.controls.idNum.value;
      this.router.navigate(['/signup', this.nextPage],
        // {
        //   state: {
        //     signupData: this.signupData,
        //     uploadFile: this.uploadFile
        //   }
        // }
        );
    }
  }
  upload(files: FileList) {
    this.uploadFile = files.item(0);
    const reader = new FileReader();
    const file = files[0];
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.photo = reader.result;
      this.validCheck();
    };
  }
  testOTP(event) {
    this.otp = event;
    if (event.length === 4) {
      this.valid = true;
    } else {
      this.valid = false;
    }
  }
  back() {
    this.location.back();
  }
  cancel() {
    sessionStorage.removeItem('email');
    sessionStorage.removeItem('mobile');
    sessionStorage.removeItem('password');
    sessionStorage.removeItem('firstName');
    sessionStorage.removeItem('lastName');
    sessionStorage.removeItem('dob');
    sessionStorage.removeItem('idNum');
    sessionStorage.removeItem('idType');
    sessionStorage.removeItem('uploadFile');
    this.router.navigate(['home']);
  }
}
