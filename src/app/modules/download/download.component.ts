import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.scss']
})
export class DownloadComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  openLink() {
    const device = this.getMobileOperatingSystem();
    if (device === 'android') {
      window.open('https://play.google.com/store/apps/details?id=asia.labx.vpass', '_blank');
    } else
    if (device === 'ios') {
      window.open('https://apps.apple.com/ph/app/labx-vpass/id1559538729', '_blank');
    } else {
      alert('App is available on iOS or Android devices only');
    }
  }

  getMobileOperatingSystem() {
    const userAgent = navigator.userAgent || navigator.vendor;

    if (/android/i.test(userAgent)) {
      return 'android';
    }

    if (/iPad|iPhone|iPod/.test(userAgent)) {
      return 'ios';
    }

    return 'unknown';
  }
}
