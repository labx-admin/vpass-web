import { Component, OnInit } from '@angular/core';
import { faQrcode, faUserCircle, faChevronLeft } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
  qrIcon = faQrcode;
  userCircle = faUserCircle;
  back = faChevronLeft;
  constructor() { }

  ngOnInit() {
  }

}
