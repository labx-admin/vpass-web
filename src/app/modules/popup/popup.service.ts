import { Injectable, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';

@Injectable({
    providedIn: 'root'
})

export class PopupService {
    invokePopup = new EventEmitter();
    subsVar: Subscription;

    constructor() {}

    popupToggle(message: string) {
        this.invokePopup.emit(message);
    }
}
