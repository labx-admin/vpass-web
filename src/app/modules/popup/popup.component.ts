import { Component, Input, OnInit } from '@angular/core';
import { PopupService } from './popup.service.js';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})
export class PopupComponent implements OnInit {
  message = 'Test Message';
  appear = '';
  constructor(private popupService: PopupService) { }

  ngOnInit() {
    if (this.popupService.subsVar === undefined) {
      this.popupService.subsVar = this.popupService.invokePopup.subscribe((message: string) => {
        this.togglePopup(message);
      });
    }
  }

  togglePopup(newMessage) {
    this.message = newMessage;
    this.appear = 'appear';
    setTimeout(() => {
      this.appear = '';
    }, 2000);
  }
}
