import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faQrcode, faUserCircle, faBell, faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import { DataService, UserDetails } from '../../services/data.service.js';
import { ApiService } from '../../services/api.service.js';
import { PopupService } from '../popup/popup.service.js';
import { NgxQrcodeElementTypes, NgxQrcodeErrorCorrectionLevels } from '@techiediaries/ngx-qrcode';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  qrIcon = faQrcode;
  userCircle = faUserCircle;
  bell = faBell;
  signOut = faSignOutAlt;
  qrCode = null;
  user: UserDetails;
  latestTest = 2;
  elementType = NgxQrcodeElementTypes.URL;
  correctionValue = NgxQrcodeErrorCorrectionLevels.HIGH;
  qrvalue = '';
  constructor(
    private dataService: DataService,
    private apiService: ApiService,
    private popupService: PopupService,
    private route: Router) { }

  ngOnInit() {
    const token = this.dataService.getToken();
    if (!token) {
      this.route.navigate(['']);
    }
    this.qrCode = this.dataService.getQr();
    this.user = this.dataService.getDetails();
    this.qrvalue = 'https://vpassph.com/result?guid=' + this.user.guid;
    this.apiService.getTestResults(token).subscribe(data => {
      if (data[0]) {
        this.latestTest = data[0].is_safe;
      }
    },
    error => {
      if (error.status === 401) {
        this.dataService.removeToken();
        this.popupService.popupToggle('Session Expired. Please login again.');
        this.route.navigate(['']);
      }
    });
    if (this.qrCode === null) {
      this.apiService.getQRCode(token).subscribe(data => {
        this.createImageFromBlob(data);
      },
        error => {
          if (error.status === 401) {
            this.dataService.removeToken();
            this.popupService.popupToggle('Session Expired. Please login again.');
            this.route.navigate(['']);
          }
        }
      );
    }
  }

  createImageFromBlob(image: Blob) {
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      this.dataService.setQr(reader.result);
      this.qrCode = reader.result;
    }, false);

    if (image) {
      reader.readAsDataURL(image);
    }
  }

  logout() {
    this.dataService.removeToken();
    this.route.navigate(['']);
  }
}
