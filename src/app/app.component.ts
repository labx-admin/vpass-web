import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'vpass';

  ngOnInit() {
    setTimeout(() => {
      const viewheight = window.outerHeight;
      const viewwidth = window.outerWidth;
      const viewport = document.querySelector('meta[name=viewport]');
      viewport.setAttribute('content', 'height=' + viewheight + ', width=' + viewwidth + ', initial-scale=1.0');
    }, 300);
  }
}
